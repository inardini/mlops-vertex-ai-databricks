#!/usr/bin/env bash

#===============================================================================
#
#          FILE:  deploy_run.sh
#
#         USAGE:  ./deploy_run.sh
#
#   DESCRIPTION: A simple script to deploy a Cloud run to Google Cloud
#
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Ivan Nardini, inardini@google.com
#       COMPANY:  Google Cloud
#       VERSION:  1.0
#       CREATED:  05/25/2007 10:31:01 PM MDT
#      REVISION:  ---
#===============================================================================

# Variables
RUN_NAME="deploy"
REGION=${REGION:-"us-central1"}
RUNTIME=python37
TRIGGER_PATH=.
#ENV_FILE_PATH=env.yaml -- seems not supported by cli even the documnentation says so
BUCKET_URI=gs://vertex-ai-databricks-retail-demo
DATABRICKS_HOST=https://2754927599824586.6.gcp.databricks.com
DATABRICKS_TOKEN=dapi613480f509e91bea1855a5718153d943
MEMORY_LIMIT=512Mi
TIMEOUT=45m


# Deploy a Cloud Function to Google Cloud
#    --env-vars-file="$ENV_FILE_PATH" \
gcloud run deploy "$RUN_NAME" \
    --region="$REGION" \
    --source="$TRIGGER_PATH" \
    --memory="$MEMORY_LIMIT" \
    --timeout="$TIMEOUT" \
    --set-env-vars="BUCKET_URI=$BUCKET_URI,DATABRICKS_HOST=$DATABRICKS_HOST,DATABRICKS_TOKEN=$DATABRICKS_TOKEN"
