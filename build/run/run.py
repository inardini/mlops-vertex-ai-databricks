#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# --------------------------------------------------
# A simple python module for Google Cloud Run
# --------------------------------------------------

# Libraries ----------------------------------------
import os
import sys
import logging
from pprint import pformat
import yaml
from yaml.loader import SafeLoader

from flask import Flask, Response, request, escape, abort
import mlflow
from mlflow import deployments

# Variables ----------------------------------------
BUCKET_URI = os.getenv('BUCKET_URI')
RUNS_URI = "/tmp/runs"
MODEL_URI = f"{BUCKET_URI}/models/"

# Client --------------------------------------------
client = deployments.get_deploy_client("google_cloud")
mlflow.set_tracking_uri(RUNS_URI)
mlflow.set_registry_uri(MODEL_URI)

app = Flask(__name__)

# Helpers -------------------------------------------
def set_logger():
    """
    Set logger for the module
    Returns:
        logger: logger object
    """
    fmt_pattern = "%(asctime)s — %(name)s — %(levelname)s —" "%(funcName)s:%(lineno)d — %(message)s"
    main_logger = logging.getLogger(__name__)
    main_logger.setLevel(logging.INFO)
    main_logger.propagate = False
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt_pattern)
    stream_handler.setFormatter(formatter)
    main_logger.addHandler(stream_handler)
    return main_logger

def get_deployment(name:str, model_uri:str, config:dict) -> dict:
    """
    Get deployment by name
    Args:
        name: deployment name
        model_uri: model uri
        config: deployment config
    Returns:
        deployment: deployment object
    """
    deployment = client.create_deployment(name, model_uri, config)
    return deployment

# Routes --------------------------------------------
@app.route("/deploy", methods=["POST"])
def deploy():
    """
    Deploy function
    Returns:
        response: response object
    """
    # Configuration ------------------------
    logger = set_logger()

    logger.info("Open the config file.")
    with open('./deployment_config.yaml') as f:
        config = yaml.load(f,
                           Loader=SafeLoader)
        logger.info(pformat(config))

    # Variables ----------------------------
    """
        Example payloads - event: MODEL_VERSION_TRANSITIONED_STAGE
        Source - https://docs.databricks.com/applications/mlflow/model-registry-webhooks.html#http-registry-webhook-example-workflow
        {
          "event": "MODEL_VERSION_TRANSITIONED_STAGE",
          "webhook_id": "c5596721253c4b429368cf6f4341b88a",
          "event_timestamp": 1589859029343,
          "model_name": "Airline_Delay_SparkML",
          "version": "8",
          "to_stage": "Production",
          "from_stage": "None",
          "text": "Registered model 'someModel' version 8 transitioned from None to Production."
        }
    """
    logger.info("Get model registry payload.")
    request_json = request.get_json(silent=True)
    if request_json:
        logger.info(pformat(request_json))
        if 'to_stage' in request_json:
            to_stage = request_json.get('to_stage')
        else:
            abort(Response(response="Model name not found", status=400))
        if 'model_name' in request_json:
            model_name = request_json.get('model_name')
        else:
            abort(Response(response="Model name not found", status=400))
        if 'version' in request_json:
            version = request_json.get('version')
        else:
            abort(Response(response="Model version not found", status=400))
    else:
        return abort(Response('Deployment request is missing.'))

    # Get the model uri
    if model_name and version:
        model_uri = f'{config["model_uri_prefix"]}{model_name}/{version}'

    else:
        abort(Response(response="Model name or version not found", status=400))

    # Deployment ---------------------------
    logging.info('Starting Deployment process')
    deployment = get_deployment(model_name, model_uri, config)
    if deployment:
        return deployment
    else:
        return abort(Response(response="Deployment failed", status=400))



