# Databricks notebook source
# MAGIC %md # Model Deployment
# MAGIC 
# MAGIC To run this notebook set the GOOGLE CREDENTIAL PATH 

# COMMAND ----------

# MAGIC %pip install google-auth google-cloud-storage google-cloud-mlflow databricks-registry-webhooks -q 

# COMMAND ----------

# MAGIC %md ## Import libraries

# COMMAND ----------

# General
import random
import string
import os
import urllib
import time

# Deployment
import mlflow
from  mlflow.tracking import MlflowClient
from mlflow.entities import ViewType
import google.oauth2.id_token
import google.auth.transport.requests
from databricks_registry_webhooks import RegistryWebhooksClient, HttpUrlSpec

# COMMAND ----------

# MAGIC %md ## Define constants

# COMMAND ----------

# General
UUID = "2obn" #Indicate the UUID created 
PROJECT_ID = "leedeb-experimentation"
REGION = "us-central1"
BUCKET_NAME = "vertex-ai-databricks-retail-demo"
BUCKET_URI = f"gs://{BUCKET_NAME}"

# COMMAND ----------

# Model Deployment 
CLOUD_FUNCTION_NAME = "deploy"
CLOUD_FUNCTION_URL = f"https://{REGION}-{PROJECT_ID}.cloudfunctions.net/{CLOUD_FUNCTION_NAME}"
EXPERIMENT_NAME = "training"
MODEL_NAME = "Weekly_Sales_GBTR_model_" + UUID
REGISTERED_MODEL = "databricks_model"
MODEL_STAGE="production"

# COMMAND ----------

# MAGIC %md ## Helpers

# COMMAND ----------

# auth function
def make_authorized_get_request(endpoint, audience):
    """
    Make an authorized request to the given endpoint.
    Args:
        endpoint: The endpoint to send the request to.
        audience: The audience to use when validating the JWT.
    Returns:
        The JSON response from the request.
    """
    # Define the request.
    req = urllib.request.Request(endpoint)

    # Get the ID token from the environment.
    auth_req = google.auth.transport.requests.Request()
    id_token = google.oauth2.id_token.fetch_id_token(auth_req, audience)
    
    return id_token

# COMMAND ----------

# MAGIC %md
# MAGIC ## Register the model

# COMMAND ----------

client = MlflowClient()

# COMMAND ----------

last_experiment = client.search_experiments()[0]
last_run = client.search_runs(last_experiment.experiment_id)[0]
last_run_id = last_run.info.run_id

# COMMAND ----------

last_run_id

# COMMAND ----------

model_uri = f"runs:/{last_run_id}/{MODEL_NAME}"
registered_model = mlflow.register_model(model_uri=model_uri, name=REGISTERED_MODEL)
time.sleep(20)

# COMMAND ----------

# add detailed comments for this specific model version that being registered
client.update_model_version(
  name=registered_model.name,
  version=registered_model.version,
  description="GBTRegressor MLLib PySpark trained for streaming pipeline, fixed issues with staging to production."
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Create the HTTP registry webhook
# MAGIC 
# MAGIC Webhooks enable you to listen for Model Registry events so your integrations can automatically trigger actions. You can use webhooks to automate and integrate your machine learning pipeline with existing CI/CD tools and workflows. 
# MAGIC 
# MAGIC For example, in our case we are going to pass the event in a cloud function and we will trigger a CI/CD builds to deploy the model on Vertex AI. 

# COMMAND ----------

# MAGIC %md
# MAGIC ### Get the auth to call the function from the webhook

# COMMAND ----------

token_id  = make_authorized_get_request(CLOUD_FUNCTION_URL, CLOUD_FUNCTION_URL)

# COMMAND ----------

http_url_spec = HttpUrlSpec(
  url=CLOUD_FUNCTION_URL,
  authorization=f"Bearer {token_id}"
)
http_webhook = RegistryWebhooksClient().create_webhook(
  model_name=registered_model.name, 
  events=["MODEL_VERSION_TRANSITIONED_STAGE"],
  http_url_spec=http_url_spec,
  description="Testing deploy model",
  status="TEST_MODE"
)


# COMMAND ----------

http_webhook

# COMMAND ----------

# MAGIC %md
# MAGIC ### Update the webhook to active status
# MAGIC To enable the webhook for real events, set its status to ACTIVE through an update call, which can also be used to change any of its other properties.

# COMMAND ----------

http_webhook = RegistryWebhooksClient().update_webhook(
  id=http_webhook.id,
  status="ACTIVE"
)

# COMMAND ----------

http_webhook

# COMMAND ----------

# MAGIC %md
# MAGIC ## Transition model to production stage

# COMMAND ----------

client.transition_model_version_stage(
  name=registered_model.name,
  version=registered_model.version,
  stage=MODEL_STAGE,
)

# COMMAND ----------

# MAGIC %md
# MAGIC ## Do not run

# COMMAND ----------

webhooks_list = RegistryWebhooksClient().list_webhooks(
  events=["MODEL_VERSION_TRANSITIONED_STAGE"]
)

# COMMAND ----------

for webhook in webhooks_list:
  RegistryWebhooksClient().delete_webhook(webhook.id)

# COMMAND ----------


