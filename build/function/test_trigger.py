#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# --------------------------------------------------
# A simple python module for testing the Cloud Funtion
# --------------------------------------------------

# Libraries ----------------------------------------
import os
import json
import urllib
import google.oauth2.id_token
import google.auth.transport.requests

REGION = "us-central1"
PROJECT_ID = "leedeb-experimentation"
FUNCTION_NAME = "deploy"
CLOUD_FUNCTION_URL = f"https://{REGION}-{PROJECT_ID}.cloudfunctions.net/{FUNCTION_NAME}"
CREDENTIAL_PATH = "./leedeb-experimentation-dbd17a0139eb.json"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = CREDENTIAL_PATH

# headersFolder={
#     "content-type": "application/json",
#     'Authorization':'bearer %s' % myToken
# }

DATA = {
    "event": "MODEL_VERSION_TRANSITIONED_STAGE",
    "webhook_id": "c5596721253c4b429368cf6f4341b88a",
    "event_timestamp": 1589859029343,
    "model_name": "Weekly_Sales_GBTR_model",
    "version": "1",
    "to_stage": "Production",
    "from_stage": "None",
    "text": "GBTRegressor MLLib PySpark trained for streaming pipeline, fixed issues with staging to production."
}


# Helper functions -------------------------------

def make_authorized_get_request(endpoint, audience):
    """
    Make an authorized request to the given endpoint.
    Args:
        endpoint: The endpoint to send the request to.
        audience: The audience to use when validating the JWT.
    Returns:
        The JSON response from the request.
    """
    # Define the request.
    req = urllib.request.Request(endpoint)

    # Get the ID token from the environment.
    auth_req = google.auth.transport.requests.Request()
    id_token = google.oauth2.id_token.fetch_id_token(auth_req, audience)

    # Add the authorization header and send the request.
    req.add_header("Authorization", f"Bearer {id_token}")

    # Add the content type header.
    req.add_header("content-type", "application/json")

    # Add the data.
    req.data = json.dumps(DATA).encode("utf-8")

    response = urllib.request.urlopen(req)

    return response.read()

print(make_authorized_get_request(CLOUD_FUNCTION_URL, CLOUD_FUNCTION_URL))
