# MLOps - Vertex AI - Databricks Demo - Continuous Deployment

The deployment plan
- Create the cloud function on Google Cloud
- Set up model registry webhook
- Test the event

Steps:
- Enable the cloud function API
- Authenticate with gcloud to the target project
- Create the cloud function using `deploy_function.sh` 
- Configure authentication
  - `gcloud functions add-iam-policy-binding deploy --region=us-central1 --member='serviceAccount:svc-databricks@leedeb-experimentation.iam.gserviceaccount.com' --role=roles/cloudfunctions.invoker`
- Make able Databricks webhook to authenticate to Cloud Function (https://cloud.google.com/functions/docs/securing/authenticating#authenticating_function_to_function_calls
