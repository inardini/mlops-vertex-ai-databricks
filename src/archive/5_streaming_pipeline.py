# Databricks notebook source
# MAGIC %md
# MAGIC # prediction using MLLib streaming from batch of CSV input files

# COMMAND ----------

# MAGIC %run ../VertexAI_Spark_MLOps/1_utils

# COMMAND ----------

# run cell below only if cluster stopped and restarted after running preprocessing step

# COMMAND ----------

# MAGIC %run ../VertexAI_Spark_MLOps/recreate_traintest_df

# COMMAND ----------

# We now repartition the test data and break them down into 10 different files and write it to a csv file.
testData = test.repartition(10)

# Remove directory in case we rerun it multiple times.
dbutils.fs.rm("FileStore/tables/streaming/",True)

# Create a directory
testData.write.format("CSV").option("header",True).save("FileStore/tables/streaming/")

# COMMAND ----------

# define schema expected by stream
schema = StructType( \
                     [StructField("Store", IntegerType(),True), \
                      StructField("Dept", IntegerType(), True), \
                      StructField("day", IntegerType(), True), \
                      StructField("month", IntegerType(), True), \
                      StructField("year", IntegerType(), True), \
                      StructField("Weekly_Sales", DoubleType(), True), \
                      StructField("IsHoliday", BooleanType(), True), \
                      StructField("Temperature", DoubleType(), True),\
                      StructField("Fuel_Price", DoubleType(), True), \
                      StructField("MarkDown1", DoubleType(), True), \
                      StructField("MarkDown2", StringType(),True), \
                      StructField("MarkDown3", StringType(), True), \
                      StructField("MarkDown4", StringType(), True), \
                      StructField("MarkDown5", StringType(), True), \
                      StructField("CPI", StringType(), True), \
                      StructField("Unemployment", StringType(), True), \
                      StructField("Type", StringType(), True), \
                      StructField("Size", DecimalType(), True), \
                      StructField("Size_double", DoubleType(), True), \
                        ])


# COMMAND ----------

# define source streaming dataframe
sourceStream = spark.readStream.format("csv").option("header",True).schema(schema).option("ignoreLeadingWhiteSpace",True).option("mode","dropMalformed").option("maxFilesPerTrigger",1).load("dbfs:/FileStore/tables/streaming").withColumnRenamed("Weekly_Sales","label")

# COMMAND ----------

# load in model using MLFlow Spark
model_uri = "runs:/"+run_id+"/"+registered_model_name
model_loaded = mlflow.spark.load_model(model_uri)

# COMMAND ----------

# apply pipeline to streaming source and display
streamingPredict = model_loaded.transform(sourceStream)
display(streamingPredict)

# COMMAND ----------


