# Databricks notebook source
# MAGIC %md
# MAGIC # data preprocessing for model 

# COMMAND ----------

# BEFORE RUNNING THIS SCRIPT:
# make sure you have already used the UI to upload the raw csv data files to the Databricks FileStore
# data files can be downloaded from kaggle here: https://www.kaggle.com/manjeetsingh/retaildataset

# COMMAND ----------

# MAGIC %run ../VertexAI_Spark_MLOps/1_utils

# COMMAND ----------

# read in data to spark df
sales_df = spark.read.csv(file_path+sales_tablename+".csv", header="true", inferSchema="true")
features_df = spark.read.csv(file_path+features_tablename+".csv", header="true", inferSchema="true")
stores_df = spark.read.csv(file_path+stores_tablename+".csv", header="true", inferSchema="true")
print("Dataframes created")

# COMMAND ----------

query_schema = 'CREATE SCHEMA {}'.format(final_dataset_schemaname)
query_table = 'DROP TABLE IF EXISTS {0}.{1}'.format(final_dataset_schemaname,final_dataset_tablename)
sqlContext.sql(query_schema)
sqlContext.sql(query_table)

# COMMAND ----------

# create temp tables from dataframes to use with SQL
sales_df.createOrReplaceTempView(sales_tablename)
features_df.createOrReplaceTempView(features_tablename)
stores_df.createOrReplaceTempView(stores_tablename)
print("Created "+sales_tablename+" "+features_tablename+" "+stores_tablename)

# COMMAND ----------

# create a table from the temp views and save as a databricks table in specified schema and table
query = 'CREATE OR REPLACE TABLE {0}.{1} AS SELECT sa.Store, sa.Dept, CAST(LEFT(sa.Date, 2) AS int) AS day, CAST(SUBSTRING(sa.Date, 4, 2) AS int) AS month, CAST(RIGHT(sa.Date, 4) AS int) AS year, sa.Weekly_Sales, sa.IsHoliday, f.Temperature, f.Fuel_Price, f.MarkDown1, f.MarkDown2, f.MarkDown3, f.MarkDown4, f.MarkDown5, f.CPI, f.Unemployment, st.Type, CAST(st.Size AS decimal) AS Size FROM {2} AS sa INNER JOIN {3} AS f on (f.Store=sa.Store AND f.Date=sa.Date) INNER JOIN {4} as st ON st.Store=sa.Store'.format(final_dataset_schemaname, final_dataset_tablename, sales_tablename, features_tablename, stores_tablename)
sqlContext.sql(query)
print("Created table "+final_dataset_schemaname+"."+final_dataset_tablename)

# COMMAND ----------

# cast Size from int to double type, create train/test df 
final_dataset_df = sqlContext.table(final_dataset_schemaname+"."+final_dataset_tablename)
final_dataset_df = final_dataset_df.withColumn("Size_double", final_dataset_df['Size'].cast("double"))

# COMMAND ----------

# split train and test data
(train, test) = final_dataset_df.randomSplit([0.8, 0.2])
print("We have "+str(train.count())+" training examples and "+str(test.count())+" test examples.")
