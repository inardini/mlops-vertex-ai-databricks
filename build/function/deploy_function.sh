#!/usr/bin/env bash

#===============================================================================
#
#          FILE:  deploy_function.sh
#
#         USAGE:  ./deploy_function.sh
#
#   DESCRIPTION: A simple script to deploy a Cloud function to Google Cloud
#
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Ivan Nardini, inardini@google.com
#       COMPANY:  Google Cloud
#       VERSION:  1.0
#       CREATED:  05/25/2007 10:31:01 PM MDT
#      REVISION:  ---
#===============================================================================

# Variables
CLOUD_FUNCTION_NAME=${1:-"deployv2"}
REGION=${2:-"us-central1"}
RUNTIME=python38
TRIGGER_PATH=.
ENV_FILE_PATH=env.yaml
MEMORY_LIMIT=2048MB
TIMEOUT=45m

# Deploy a Cloud Function to Google Cloud
#gcloud functions deploy "$CLOUD_FUNCTION_NAME" \
#    --region="$REGION" \
#    --runtime="$RUNTIME" \
#    --source="$TRIGGER_PATH" \
#    --env-vars-file="$ENV_FILE_PATH" \
#    --memory="$MEMORY_LIMIT" \
#    --trigger-http

gcloud functions deploy "$CLOUD_FUNCTION_NAME" \
    --gen2 \
    --region="$REGION" \
    --runtime="$RUNTIME" \
    --source="$TRIGGER_PATH" \
    --entry-point="deploy" \
    --env-vars-file="$ENV_FILE_PATH" \
    --memory="$MEMORY_LIMIT" \
    --timeout="$TIMEOUT" \
    --trigger-http