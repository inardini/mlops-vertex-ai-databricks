# Databricks notebook source
# MAGIC %run ../VertexAI_Spark_MLOps/1_utils

# COMMAND ----------

# note: change registered model name and runID as variables in utils file if needed
print("Registering model "+registered_model_name+" from runID "+run_id)

# COMMAND ----------

# register model - PANDAS NOT SPARK, IGNORE
model_uri = "runs:/"+run_id+"/"+registered_model_name
model_details = mlflow.register_model(model_uri=model_uri, name=registered_model_name)
time.sleep(20)

# COMMAND ----------

# add detailed comments for this specific model version that being registered
client = MlflowClient()
client.update_model_version(
  name=model_details.name,
  version=model_details.version,
  description="GBTRegressor MLLib PySpark trained for streaming pipeline, fixed issues with staging to production."
)

# COMMAND ----------

# transition model to production stage
time.sleep(20)
client.transition_model_version_stage(
  name=model_details.name,
  version=model_details.version,
  stage='Production',
)
model_version_uri = "models:/{model_name}/production".format(model_name=registered_model_name)
print("Loading PRODUCTION model stage with name: '{model_uri}'".format(model_uri=model_version_uri))
