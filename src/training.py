# Databricks notebook source
# MAGIC %md # Training a simple Sklearn model

# COMMAND ----------

# MAGIC %md ## Import libraries

# COMMAND ----------

# General
import random
import string

# Preprocessing
from pyspark import *

# Training
import mlflow
from pyspark.ml.feature import FeatureHasher, VectorAssembler
from pyspark.ml.regression import GBTRegressor
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import RegressionEvaluator

# COMMAND ----------

# MAGIC %md ## Helpers

# COMMAND ----------

def generate_uuid(length: int = 8) -> str:
    return "".join(random.choices(string.ascii_lowercase + string.digits, k=length))

# COMMAND ----------

# MAGIC %md ## Define constants

# COMMAND ----------

UUID = generate_uuid(4)

# COMMAND ----------

# Data ingestion
FILE_PATH = "/FileStore/tables/rawfiles/"
SALES_TABLE_NAME = f"sales_data_set"
FEATURES_TABLE_NAME = f"features_data_set"
STORES_TABLE_NAME = f"stores_data_set"

FINAL_DATASET_SCHEMA_NAME = "databricks_vertex"
FINAL_DATASET_TABLE_NAME = f"kaggle_retail_dataset_{UUID}"
MODEL_NAME = f"Weekly_Sales_GBTR_model_{UUID}"

# COMMAND ----------

# MAGIC %md ## Ingest data

# COMMAND ----------

sales_df = spark.read.csv(FILE_PATH+SALES_TABLE_NAME+".csv", header="true", inferSchema="true")
features_df = spark.read.csv(FILE_PATH+FEATURES_TABLE_NAME+".csv", header="true", inferSchema="true")
stores_df = spark.read.csv(FILE_PATH+STORES_TABLE_NAME+".csv", header="true", inferSchema="true")

# COMMAND ----------

query_schema = 'CREATE SCHEMA IF NOT EXISTS {}'.format(FINAL_DATASET_SCHEMA_NAME)
query_table = 'DROP TABLE IF EXISTS {0}.{1}'.format(FINAL_DATASET_TABLE_NAME,FINAL_DATASET_TABLE_NAME)
sqlContext.sql(query_schema)
sqlContext.sql(query_table)

# COMMAND ----------

sales_df.createOrReplaceTempView(SALES_TABLE_NAME+UUID)
features_df.createOrReplaceTempView(FEATURES_TABLE_NAME+UUID)
stores_df.createOrReplaceTempView(STORES_TABLE_NAME+UUID)

# COMMAND ----------

query = f"CREATE OR REPLACE TABLE {FINAL_DATASET_SCHEMA_NAME}.{FINAL_DATASET_TABLE_NAME} AS SELECT sa.Store, sa.Dept, CAST(LEFT(sa.Date, 2) AS int) AS day, CAST(SUBSTRING(sa.Date, 4, 2) AS int) AS month, CAST(RIGHT(sa.Date, 4) AS int) AS year, sa.Weekly_Sales, sa.IsHoliday, f.Temperature, f.Fuel_Price, f.MarkDown1, f.MarkDown2, f.MarkDown3, f.MarkDown4, f.MarkDown5, f.CPI, f.Unemployment, st.Type, CAST(st.Size AS decimal) AS Size FROM {SALES_TABLE_NAME+UUID} AS sa INNER JOIN {FEATURES_TABLE_NAME+UUID} AS f on (f.Store=sa.Store AND f.Date=sa.Date) INNER JOIN {STORES_TABLE_NAME+UUID} as st ON st.Store=sa.Store"
sqlContext.sql(query)

# COMMAND ----------

preprocessed_df = sqlContext.table(FINAL_DATASET_SCHEMA_NAME+"."+FINAL_DATASET_TABLE_NAME)
preprocessed_df = preprocessed_df.withColumn("Size_double", preprocessed_df['Size'].cast("double"))

# COMMAND ----------

# MAGIC %md ## Training-Test splitting

# COMMAND ----------

(train, test) = preprocessed_df.randomSplit([0.8, 0.2])

# COMMAND ----------

# MAGIC %md ## Model Training

# COMMAND ----------

# enable MLFlow autologging
mlflow.spark.autolog()

# start MLFlow run
with mlflow.start_run(run_name='Weekly_Sales_GBTR'+UUID) as run:
    
    # add hasher
    hasher = FeatureHasher(inputCols=['IsHoliday', 'day', 'month', 'year', 'Temperature', 'Fuel_Price', 'Size_double', 'Store', 'Dept', 'MarkDown1', 'MarkDown2', 'MarkDown3', 'MarkDown4', 'MarkDown5', 'CPI', 'Unemployment', 'Type'],
                       outputCol="features")
    
    # add assembler
    feat_cols=["features"]
    assembler = VectorAssembler(inputCols=feat_cols, outputCol="features_dense")
    
    # model definition with parameters
    gbtr = GBTRegressor(featuresCol='features_dense', labelCol='Weekly_Sales', maxIter=1)
    
    # define pipeline
    pipeline = Pipeline(stages=[hasher, assembler, gbtr])
    
    # training pipeline
    gbtr_model = pipeline.fit(train)
    
    # predictions on test data
    gbtr_predictions = gbtr_model.transform(test)

    # log model
    mlflow.spark.log_model(gbtr_model, MODEL_NAME)
  
    # log the rmse
    rmse=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="rmse")
    rmse=rmse.evaluate(gbtr_predictions) 
    mlflow.log_metric("RMSE", rmse)
    
    # log mae
    mae=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="mae")
    mae=mae.evaluate(gbtr_predictions) 
    mlflow.log_metric("MAE", mae)
    
    # log r^2
    r2=RegressionEvaluator(labelCol="Weekly_Sales", predictionCol="prediction", metricName="r2")
    r2=r2.evaluate(gbtr_predictions)
    mlflow.log_metric("R-Square", r2)
    
mlflow.end_run()
